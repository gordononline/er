import recordlinkage
from recordlinkage.index import Block
import pandas as pd

# Hypothetical datasets
data1 = {
    "Name": ["David Smith", "Bill Johnson", "Charles Brown"],
    "DateOfBirth": ["1990-01-01", "1985-05-12", "1980-07-23"],
    "Gender": ["F", "M", "M"],
    "Address": ["123 Main St", "456 Elm St", "789 Oak St"],
}

data2 = {
    "Name": ["David Smith", "Robert Johnson", "Chad Brade"],
    "DateOfBirth": ["1990-01-01", "1985-05-12", "1980-07-25"],
    "Gender": ["F", "M", "M"],
    "Address": ["123 Main Street", "456 Elm Street", "789 Oak Street"],
}

df1 = pd.DataFrame(data1)
df2 = pd.DataFrame(data2)

# Build an index - use 'Name' as the blocking key
indexer = Block("Name")
candidate_links = indexer.index(df1, df2)

# Initialize the comparison object
compare = recordlinkage.Compare()

# Add comparison rules
compare.string("Name", "Name", method="jarowinkler", threshold=0.85, label="Name")
compare.exact("Gender", "Gender", label="Gender")
compare.string(
    "DateOfBirth",
    "DateOfBirth",
    method="jarowinkler",
    threshold=0.85,
    label="DateOfBirth",
)
compare.string(
    "Address", "Address", method="jarowinkler", threshold=0.85, label="Address"
)

# Compute features
features = compare.compute(candidate_links, df1, df2)

# Identify matches
matches = features[features.sum(axis=1) > 3].reset_index()
matches = matches.set_index(["level_0", "level_1"])

# Output match results
print("Matched Records:")
for index in matches.index:
    print(f"Data1 record: {index[0]} matches Data2 record: {index[1]}")
